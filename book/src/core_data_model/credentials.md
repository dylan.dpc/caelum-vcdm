# Credentials

![](https://www.w3.org/TR/vc-data-model/diagrams/credential.svg)

> A credential is a set of one or more claims made by the same entity. Credentials might also include an 
identifier and metadata to describe properties of the credential, such as the issuer, the expiry date and
time, a representative image, a public key to use for verification purposes, the revocation mechanism, and
so on. The metadata might be signed by the issuer. A verifiable credential is a set of tamper-evident claims
and metadata that cryptographically prove who issued it.

![](https://www.w3.org/TR/vc-data-model/diagrams/credential-graph.svg)