## Contributing

Please, contribute to `vcdm`! The more the better! Feel free to to open an issue and/or contacting directly with the 
owner for any request or suggestion.

## Code of conduct
This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at 
[http://contributor-covenant.org/version/1/4][version]
