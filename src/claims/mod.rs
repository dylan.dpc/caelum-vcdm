extern crate hex;
use ed25519_dalek::{Keypair, PublicKey, Signature};

use base64;
use bytes::Bytes;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

pub mod credential_status;
pub mod credential_subject;
use credential_status::CredentialStatus;
use credential_subject::CredSubject;

use crate::proofs::proof::Proof;
use crate::utils::{create_hash, hex_to_u8a};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Claim {
    #[serde(rename(serialize = "credentialStatus", deserialize = "credentialStatus"))]
    pub credential_status: CredentialStatus,
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub credential_type: Vec<String>,
    #[serde(rename(serialize = "credentialSubject", deserialize = "credentialSubject"))]
    pub credential_subject: Vec<CredSubject>,
    pub id: String,
    #[serde(rename(serialize = "issuanceDate", deserialize = "issuanceDate"))]
    pub issuance_date: String,
    pub issuer: String,
}

impl Claim {
    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    // GENERATOR
    pub fn to_json(&self) -> JsValue {
        JsValue::from_serde(&self).unwrap()
    }

    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn sign(&self, keys: &[u8]) -> String {
        // Create Keypair from keys
        // TODO: change unwrap() in favor of checking results
        let keypair = Keypair::from_bytes(&keys[0..64]).unwrap();
        // Hash of claim (self)
        let hashed_msg = create_hash(&self.to_str());
        // Sign hashed credential with keypair
        let signature: Signature = keypair.sign(hashed_msg.as_bytes());
        // Change base of signed credential (to b64)
        let b = Bytes::from(&signature.to_bytes()[..]);
        base64::encode(&b)
    }

    pub fn verify(&self, proof: &Proof) -> bool {
        match verify_claim(&self, &proof) {
            Ok(_) => true,
            _ => false,
        }
    }
}

impl Default for Claim {
    fn default() -> Self {
        Claim {
            credential_type: vec!["VerifiableCredential".to_string()],
            credential_status: CredentialStatus::default(),
            credential_subject: Vec::new(),
            id: "".to_string(),
            issuance_date: "".to_string(),
            issuer: "".to_string(),
        }
    }
}

pub fn verify_claim(claim: &Claim, proof: &Proof) -> Result<(), String> {
    // Public Key
    let u8a_pk = hex_to_u8a(&proof.verification_method);
    let public_key = match PublicKey::from_bytes(&u8a_pk) {
        Ok(a) => a,
        Err(e) => return Err(format!("Invalid public key: {:?}.", e)),
    };
    // Change of base base64 -> normal Same effect as in javascript
    let unbased64_msg = &base64::decode(&proof.signature_value).unwrap()[..];

    // Decrypt proof.signatureValue with issuer's publicKey,
    let signed_msg: Signature = Signature::from_bytes(unbased64_msg).unwrap();

    // Hash of claim
    let hash_hex_vec = create_hash(&claim.to_str());

    match public_key.verify(hash_hex_vec.as_bytes(), &signed_msg) {
        Ok(_) => Ok(()),
        _ => Err("Claim and Proof do not coincide.".to_string()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_claim_basic() {
        let default_claim = Claim::default();

        let claim_json_str = r#"{
                "id": "",
                "credentialStatus": {
                    "id": "",
                    "type": "",
                    "currentStatus": "",
                    "statusReason": ""
                },
                "credentialSubject": [],
                "issuer": "",
                "issuanceDate": "",
                "type": ["VerifiableCredential"]
            }"#;
        let claim_from_json: Claim = serde_json::from_str(claim_json_str).unwrap();

        assert_eq!(claim_from_json.to_str(), default_claim.to_str());
    }

    #[test]
    fn test_claim_sign() {
        // Create Keypair
        let keypair = vec![
            212, 109, 1, 120, 214, 102, 78, 169, 141, 239, 187, 76, 224, 61, 74, 250, 20, 4, 89,
            89, 159, 113, 116, 168, 87, 79, 196, 25, 155, 87, 180, 134, 65, 189, 48, 58, 3, 235, 0,
            194, 39, 238, 5, 140, 57, 222, 169, 56, 62, 89, 169, 66, 225, 253, 97, 6, 241, 72, 72,
            72, 195, 209, 12, 81,
        ];

        // Create Claim
        let claim: Claim = serde_json::from_str(r#"{"credentialStatus":{"id":"https://lorena.caelumlabs.com/credentials","type":"CaelumEmailCredential","currentStatus":"pending","statusReason":"self-issued"},"type":["VerifiableCredential","CaelumEmailCredential"],"credentialSubject":[{"email":"asd","last":"","type":"CaelumEmailCredential","first":""}],"id":"Hash: 0xa76745d559991b293036244416c6818b15d8debf6960151d4c6abf5b284fbb1b","issuanceDate":"2019-10-16T14:07:41.657Z","issuer":"did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2"}"#).unwrap();

        // sign Claim
        let signature_b64 = claim.sign(&keypair);

        assert_eq!(
            signature_b64,
            "LAVMIj9X59o3TL0CtfEWbUT8nUBnHaUwYdE0wVGTlA6yoaBe+OF7yE3c8pLwbYwESYFYHjX7Zsd1/iy6csrSDA==".to_string()
        );
        // Keypair: [212, 109, 1, 120, 214, 102, 78, 169, 141, 239, 187, 76, 224, 61, 74, 250, 20, 4, 89, 89, 159, 113, 116, 168, 87, 79, 196, 25, 155, 87, 180, 134, 65, 189, 48, 58, 3, 235, 0, 194, 39, 238, 5, 140, 57, 222, 169, 56, 62, 89, 169, 66, 225, 253, 97, 6, 241, 72, 72, 72, 195, 209, 12, 81]
        // claim: {"credentialStatus":{"id":"https://lorena.caelumlabs.com/credentials","type":"CaelumEmailCredential","currentStatus":"pending","statusReason":"self-issued"},"type":["VerifiableCredential","CaelumEmailCredential"],"credentialSubject":[{"email":"asd","last":"","type":"CaelumEmailCredential","first":""}],"id":"Hash: 0xa76745d559991b293036244416c6818b15d8debf6960151d4c6abf5b284fbb1b","issuanceDate":"2019-10-16T14:07:41.657Z","issuer":"did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2"}
        // hash of claim: 80464a1d977d369f577d88710cd7f9d3c2278301c268eeab7b89888f68e84fb32d2896c72abc3a8dbaa4c295f66100ac6727916c73c2bea16e86bfcff4365591
        // final signature: [20, 43, 243, 210, 195, 178, 255, 149, 243, 64, 46, 59, 46, 12, 150, 14, 239, 73, 70, 9, 111, 183, 100, 157, 65, 139, 90, 245, 89, 176, 44, 8, 230, 39, 111, 242, 163, 151, 161, 6, 169, 9, 123, 100, 180, 157, 60, 90, 160, 123, 167, 190, 253, 184, 20, 85, 19, 47, 178, 129, 105, 52, 216, 7]
    }

    #[test]
    fn test_verify_claim() {
        let c: Claim = serde_json::from_str(r#"{"credentialStatus":{"id":"https://lorena.caelumlabs.com/credentials","type":"CaelumEmailCredential","currentStatus":"pending","statusReason":"self-issued"},"type":["VerifiableCredential","CaelumEmailCredential"],"credentialSubject":[{"email":"asd","last":"","type":"CaelumEmailCredential","first":""}],"id":"Hash: 0xa76745d559991b293036244416c6818b15d8debf6960151d4c6abf5b284fbb1b","issuanceDate":"2019-10-16T14:07:41.657Z","issuer":"did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2"}"#).unwrap();
        let p: Proof = Proof::from_claim_and_keys(
            &c,
            &vec![
                212, 109, 1, 120, 214, 102, 78, 169, 141, 239, 187, 76, 224, 61, 74, 250, 20, 4,
                89, 89, 159, 113, 116, 168, 87, 79, 196, 25, 155, 87, 180, 134, 65, 189, 48, 58, 3,
                235, 0, 194, 39, 238, 5, 140, 57, 222, 169, 56, 62, 89, 169, 66, 225, 253, 97, 6,
                241, 72, 72, 72, 195, 209, 12, 81,
            ],
        );

        assert_eq!(verify_claim(&c, &p), Ok(()));
    }
}
