use serde::{Deserialize, Serialize};

use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ZenroomSignature {
    r: String,
    s: String,
}
impl Default for ZenroomSignature {
    fn default() -> Self {
        ZenroomSignature {
            r: "".to_string(),
            s: "".to_string(),
        }
    }
}
impl ZenroomSignature {
    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ZenroomProof {
    draft: String,
    signature: ZenroomSignature,
}
impl Default for ZenroomProof {
    fn default() -> Self {
        ZenroomProof {
            draft: "".to_string(),
            signature: ZenroomSignature::default(),
        }
    }
}
impl ZenroomProof {
    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NonRevocationProof {
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub auth_type: String,
    pub created: String,
    #[serde(rename(serialize = "verificationMethod", deserialize = "verificationMethod"))]
    pub verification_method: String,
    #[serde(rename(serialize = "signatureValue", deserialize = "signatureValue"))]
    pub signature_value: String,
    pub zenroom: ZenroomProof,
}

impl Default for NonRevocationProof {
    fn default() -> Self {
        NonRevocationProof {
            auth_type: "".to_string(),
            created: "".to_string(),
            verification_method: "".to_string(),
            signature_value: "".to_string(),
            zenroom: ZenroomProof::default(),
        }
    }
}

impl NonRevocationProof {
    pub fn new(
        auth_type: String,
        created: String,
        verification_method: String,
        signature_value: String,
        zenroom: ZenroomProof,
    ) -> Self {
        NonRevocationProof {
            auth_type,
            created,
            verification_method,
            signature_value,
            zenroom,
        }
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn to_json(&self) -> JsValue {
        JsValue::from_serde(&self).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_zenroom_signature() {
        let nrp: ZenroomSignature = serde_json::from_str(r#"{
                "r": "u64:HeYdIQ2QY3qdMVtZshqN-vOqeVnh1z6VC5LhkwizVadMONKrWPT08U8cN1hZeH-wQHY8_w4Xbwg",
                "s": "u64:ExnZWn0keKZrTcICVIPrVdWzWiS7pu2aKdIuOt407NbPGaJJBhiPwxAT2speEBc7VtXEqhyPSjs"
        }"#).unwrap();
        println!("{:#?}", nrp)
    }

    #[test]
    fn test_zenroom_proof() {
        let nrp: ZenroomProof = serde_json::from_str(r#"{
            "draft": "u64:W29iamVjdF9PYmplY3Rd",
            "signature": {
                "r": "u64:HeYdIQ2QY3qdMVtZshqN-vOqeVnh1z6VC5LhkwizVadMONKrWPT08U8cN1hZeH-wQHY8_w4Xbwg",
                "s": "u64:ExnZWn0keKZrTcICVIPrVdWzWiS7pu2aKdIuOt407NbPGaJJBhiPwxAT2speEBc7VtXEqhyPSjs"
            }
        }"#).unwrap();
        println!("{:#?}", nrp)
    }

    #[test]
    fn test_non_revocation_proof() {
        let nrp: NonRevocationProof = serde_json::from_str(
            r#"{
            "type": "",
            "created": "",
            "verificationMethod": "",
            "signatureValue": "",
            "zenroom": {
                "draft": "",
                "signature": {
                    "r": "",
                    "s": ""
                }
            }
        }"#,
        )
        .unwrap();
        println!("{:#?}", nrp)
    }
}
