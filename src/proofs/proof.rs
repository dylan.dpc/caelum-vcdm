use serde::{Deserialize, Serialize};

use wasm_bindgen::prelude::*;

use crate::claims::Claim;
use crate::utils::get_utc_time;
use crate::utils::u8a_to_hex;

extern crate chrono;
extern crate stdweb;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Proof {
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub auth_type: String,
    pub created: String,
    #[serde(rename(serialize = "verificationMethod", deserialize = "verificationMethod"))]
    pub verification_method: String,
    #[serde(rename(serialize = "signatureValue", deserialize = "signatureValue"))]
    pub signature_value: String,
}

impl Default for Proof {
    fn default() -> Self {
        Proof {
            auth_type: "".to_string(),
            created: "".to_string(),
            verification_method: "".to_string(),
            signature_value: "".to_string(),
        }
    }
}

impl Proof {
    pub fn new(
        auth_type: String,
        created: String,
        verification_method: String,
        signature_value: String,
    ) -> Self {
        Proof {
            auth_type,
            created,
            verification_method,
            signature_value,
        }
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    pub fn from_claim_and_keys(claim: &Claim, keys: &[u8]) -> Self {
        // let utc: DateTime<Utc> = Utc::now();       // e.g. `2014-11-28T12:45:59.324310806Z`
        // let local: DateTime<Local> = Local::now(); // e.g. `2014-11-28T21:45:59.324310806+09:00`
        Proof {
            auth_type: "default".to_string(),
            created: get_utc_time(),
            verification_method: u8a_to_hex(&keys[32..64]).to_string(),
            signature_value: claim.sign(keys).to_string(),
        }
    }

    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn to_json(&self) -> JsValue {
        JsValue::from_serde(&self).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_proof_from_claim_and_keys() {
        let keys = vec![
            212, 109, 1, 120, 214, 102, 78, 169, 141, 239, 187, 76, 224, 61, 74, 250, 20, 4, 89,
            89, 159, 113, 116, 168, 87, 79, 196, 25, 155, 87, 180, 134, 65, 189, 48, 58, 3, 235, 0,
            194, 39, 238, 5, 140, 57, 222, 169, 56, 62, 89, 169, 66, 225, 253, 97, 6, 241, 72, 72,
            72, 195, 209, 12, 81,
        ];
        let claim: Claim = serde_json::from_str(r#"{"credentialStatus":{"id":"https://lorena.caelumlabs.com/credentials","type":"CaelumEmailCredential","currentStatus":"pending","statusReason":"self-issued"},"type":["VerifiableCredential","CaelumEmailCredential"],"credentialSubject":[{"email":"asd","last":"","type":"CaelumEmailCredential","first":""}],"id":"Hash: 0xa76745d559991b293036244416c6818b15d8debf6960151d4c6abf5b284fbb1b","issuanceDate":"2019-10-16T14:07:41.657Z","issuer":"did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2"}"#).unwrap();

        let a = Proof::from_claim_and_keys(&claim, &keys);
        assert_eq!(
            &a.signature_value,
            "LAVMIj9X59o3TL0CtfEWbUT8nUBnHaUwYdE0wVGTlA6yoaBe+OF7yE3c8pLwbYwESYFYHjX7Zsd1/iy6csrSDA=="
        );
        assert_eq!(
            &a.verification_method,
            "41bd303a03eb00c227ee058c39dea9383e59a942e1fd6106f1484848c3d10c51"
        );
    }
}
